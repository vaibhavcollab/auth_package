<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Support\Facades\DB;

class AddColumnsToUsersTable extends Migration
{

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (Schema::hasTable('users')) {
                DB::statement(
                    'ALTER TABLE users '
                    .'ADD COLUMN mobile_no VARCHAR(15) NULL AFTER email,'
                    .'ADD COLUMN firm_name VARCHAR(100) NULL AFTER mobile_no, '
                    .'ADD COLUMN company_id INT NULL AFTER id, '
                    .'ADD COLUMN role_id INT NULL AFTER company_id, '
                    .'ADD COLUMN image_path text NULL AFTER firm_name, '
                    .'ADD COLUMN language_id varchar(10) NULL after image_path, '
                    .'ADD COLUMN language_code varchar(10) NULL after language_id, '
                    .'ADD COLUMN language_description VARCHAR(50) NULL after language_code,  '
                    .'ADD COLUMN profile_pic text NULL after language_description, '
                    .'ADD COLUMN status_code varchar(10) NULL after profile_pic, '
                    .'ADD COLUMN status_description varchar(50) NULL after status_code, '
                    .'ADD COLUMN referral_id varchar(100) NULL after status_description, '
                    .'ADD COLUMN make_default TINYINT(1) DEFAULT 0 NULL after referral_id, '
                    .'ADD COLUMN is_deleted tinyint(1) DEFAULT 0 NULL after remember_token, '
                    .'ADD COLUMN is_completed tinyint(1) DEFAULT 0 NULL after is_deleted, '
                    .'ADD COLUMN created_by INT  NULL after is_completed, '
                    .'ADD COLUMN updated_by INT  NULL after created_by, '
                    .'CHANGE created_at created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL, '
                    .'CHANGE updated_at updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL;'
                );

            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('users', function ($table) {
                $table->dropColumn([
                    // 'mobile_no',
                    // 'firm_name',
                    // 'company_id',
                    // 'image_path',
                    // 'language_code',
                    // 'language_description',
                    // 'language_id',
                    // 'profile_pic',
                    // 'referral_id',
                    // 'is_deleted',
                    // 'is_completed',
                    // 'status_code',
                    // 'status_description',
                    // 'make_default'
                ]);
            });
        }


}
