<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name', 255)->nullable();
            $table->string('company_logo', 255)->nullable();
            $table->string('company_address_type', 20)->nullable();
            $table->string('company_address_description', 255)->nullable();
            $table->integer('parent_company_id')->nullable();
            $table->string('address_line_1', 255)->nullable();
            $table->string('address_line_2', 255)->nullable();
            $table->string('address_line_3', 255)->nullable();
            $table->string('city_code', 20)->nullable();
            $table->string('city_description', 255)->nullable();
            $table->string('state_code', 20)->nullable();
            $table->string('state_description', 255)->nullable();
            $table->integer('pin')->nullable();
            $table->string('pan_no', 20)->nullable();
            $table->string('gst_no', 15)->nullable();
            $table->string('prospect_classification_code', 20)->nullable();
            $table->string('prospect_classification_description', 255)->nullable();
            $table->string('primary_classification_code', 20)->nullable();
            $table->string('primary_classification_description', 255)->nullable();
            $table->string('contact_classification_code', 20)->nullable();
            $table->string('contact_classification_description', 255)->nullable();
            $table->tinyInteger('is_lead')->default('0')->nullable();
            $table->string('lead_source', 20)->nullable();
            $table->string('lead_source_description', 255)->nullable();
            $table->string('source_details_code', 20)->nullable();
            $table->string('source_details_description', 255)->nullable();
            $table->string('lost_reason_code', 20)->nullable();
            $table->string('lost_reason_description', 255)->nullable();
            $table->integer('primary_contact_person')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('is_deleted')->default('0');
            $table->tinyInteger('is_completed')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
