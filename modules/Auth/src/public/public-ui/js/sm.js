$(document).ready(function() {
    // Mobile Number Validation on Registration form.
    $(".mobile_number_validation").on("keypress keyup blur", function(event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (this.value.length == 0 && event.which == 48) {
            return false;
        }
    });
});


function rental_category_search() {
    try {
        $(function() {
            $("#rental-search").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: APP_URL + '/findMachineCategory',
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function(data) {
                            response(data.data.list);
                        }
                    });
                },
                minLength: 2,
                focus: function(event, ui) {
                    $("#rental-search").val(ui.item.name);
                    $('#rental-category').val(ui.item.formatted_name);
                    return false;
                },
                select: function(event, ui) {
                    $("#rental-search").val(ui.item.name);
                    $('#rental-category').val(ui.item.formatted_name);
                    return false;
                }
            }).autocomplete("instance")._renderItem = function(ul, item) {
                var text = $("#rental-search").val();
                var item = $('<div class="list_item_container border-bottom"><div class="bold">' + text + '</div><div class="text-warning1">in ' + item.name + '</div></div>')
                return $("<li>").append(item).appendTo(ul);
            };
        });
    } catch (err) {
        console.log(err);
    }
}

function rental_search_submit() {
    $("#rental-form").submit(function() {
        var rentalCategory = $('#rental-category').val();
        var rentalLocation = $('#rental-city-name').val();
        //console.log(rentalLocation, rentalCategory);
        //window.location.replace("{{url('/')}}" + '/rental-search/' + rentalCategory + '-in-' + rentalLocation.toLowerCase());
        return false;
    });

    $('#submit-rental').on('click', function() {
        var rentalCategory = $('#rental-category').val();
        var rentalLocation = $('#rental-city-name').val();
        if (rentalCategory !== '' && rentalLocation !== '') {
            $('loader').show();
            window.location.replace(APP_URL + '/rental-listing/' + rentalCategory + '-on-rent-in-' + rentalLocation.toLowerCase());
        }
    });
}

function resale_category_search() {
    try {
        $(function() {
            $("#resale-search").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: APP_URL + '/findMachineCategory',
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function(data) {
                            response(data.data.list);
                        }
                    });
                },
                minLength: 2,
                focus: function(event, ui) {
                    $("#resale-search").val(ui.item.name);
                    $('#resale-category').val(ui.item.formatted_name);
                    return false;
                },
                select: function(event, ui) {
                    $("#resale-search").val(ui.item.name);
                    $('#resale-category').val(ui.item.formatted_name);
                    return false;
                }
            }).autocomplete("instance")._renderItem = function(ul, item) {
                var text = $("#resale-search").val();
                var item = $('<div class="list_item_container border-bottom"><div class="bold">' + text + '</div><div class="text-warning1">in ' + item.name + '</div></div>')
                return $("<li>").append(item).appendTo(ul);
            };
        });
    } catch (err) {
        console.log(err);
    }
}

function resale_search_submit() {
    $("#resale-form").submit(function() {
        var rentalCategory = $('#resale-category').val();
        var rentalLocation = $('#resale-city-name').val();
        //console.log(rentalLocation, rentalCategory);
        //window.location.replace("{{url('/')}}" + '/rental-search/' + rentalCategory + '-in-' + rentalLocation.toLowerCase());
        return false;
    });

    $('#submit-resale').on('click', function() {
        var rentalCategory = $('#resale-category').val();
        var rentalLocation = $('#resale-city-name').val();
        if (rentalCategory !== '' && rentalLocation !== '') {
            $('loader').show();
            window.location.replace(APP_URL + '/resale-listing/buy-' + rentalCategory + '-in-' + rentalLocation.toLowerCase());
        }
    });
}
