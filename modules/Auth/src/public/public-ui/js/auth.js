$(document).ready(function(){
    /*Send OTP AJAX Call*/
    $('#send-otp').on('click', function () {
        var user_id = $("#uer_id").val();
        if(user_id != ''){
            $('#divLoading, .loader').show();
            $.ajax({
                type: 'GET',
                url: APP_URL + '/sendOTP',
                data: {'userID': user_id},
                success: function (data) {
                    if(data.hasOwnProperty("success")){
                        $('#divLoading, .loader').hide();
                        //alert(data.success.otp);
                        $('#password').attr('name', 'otp');
                        $('#email').val(data['user_id']);
                        $('#password').attr('placeholder', 'Enter OTP');
                        $('#login_with_otp_Modal').modal('hide');
                        setTimeout(function() {
                            $('#password').focus();
                        }, 500);
                    } else {
                        $("#error_msg").html(data.exception.global)
                        $("#error_msg").css("color", "red");
                        $('#divLoading, .loader').hide();
                    }
                }, error: function(err) {
                    $('#divLoading, .loader').hide();
                }
            });
        } else {
            $('#error_msg').html('Required');
        }
    });

    $('#login_with_otp').on('click', function() {
        var btnText = $(this).val();
        if(btnText === 'Login with OTP') {
            $('#login_with_otp_Modal').modal('show');
        } else {
            $('#password').attr('name', 'password');
            $('#password').attr('placeholder', 'Enter Password');
            $(this).val('Login with OTP');
            history.pushState('Login', '', APP_URL + '/login');
        }
    });

    $('#login-form').submit(function() {
        var action = $(this).attr('action');
        if(action == APP_URL + '/check-user-companies') {
            $.ajax({
                url: action,
                type: "post",
                data: $('#login-form').serialize(),
                success: function(response) {
                    response = JSON.parse(response);
                    if(response.status === 'companies') {
                        $('#company-count').val(response.count);
                        $select = $('#companies');
                        $select.html('<option value="">Select</option>');
                        $.each(response.list, function(key, value) {
                            $select.append('<option value=' + value.company_id + '>' + value.company_name + '</option>');
                        });
                        $('#company-dropdown-modal').modal('show');
                    }
                    if(response.status === 'user') {
                        $('#login-form').attr('action',APP_URL + '/login');
                        $('#login-form').submit();
                    }
                    if(response.status === 'default') {
                        $('#company-id').val(response.company_id);
                        $('#login-form').attr('action',APP_URL + '/login');
                        $('#login-form').submit();
                    }
                    if(response.status === 'false') {
                        $(".errorEmail").text(response.message);
                        $("#email").addClass("is-invalid");
                        $("#password").addClass("is-invalid");
                    }
                }
            });
            return false;
        } else {
            return true;
        }
    });

    $('.company-chosen-submit').on('click', function() {
        var companyID   = $('#companies').val();
        var makeDefault = $('#user-make-default').is(':checked') ? '1' : '0';
        if(companyID == '') {
            $('#select-company-msg').text('Please select company.');
        } else {
            $('#company-id').val(companyID);
            $('#make-default').val(makeDefault);
            $('#login-form').attr('action',APP_URL + '/login');
            $('#company-dropdown-modal').modal('hide');
            $('#login-form').submit();
        }
    });

    $('#companies').on('change', function() {
        var companyID = $('#companies').find(":selected").val();
        if(companyID === '') {
            $('#select-company-msg').text('Please select company.');
        } else {
            $('#select-company-msg').text('');
        }
    });
});
