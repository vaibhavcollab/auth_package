<?php

namespace App\Modules\Auth\src\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class LoggingReport extends Authenticatable
{
	 //Use Updater;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'user_name', 'user_token', 'start_time', 'end_time', 'media', 'username_type', 'password_type', 'created_by', 'updated_by', 'created_at', 'updated_at', 'session_type'];
    public $table       = 'login_report';
}
