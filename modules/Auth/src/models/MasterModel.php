<?php

namespace App\Modules\Auth\src\Models;

use App\common\Common;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MasterModel extends Model
{
    // get master data
    public function getMasterDetails($table_name)
    {
        $data = array();
        $data = DB::table($table_name)->paginate(5);
        return $data;
    }

    public function getSimpleDetails($search_string, $table_name, $search_column)
    {
        try{
            $data = array();
            $data = "";
            /* echo "<pre>".$search_string;
            echo "<pre>".$table_name;*/
            $data = DB::table($table_name)->where('code', 'like', '%' . $search_string . '%')->orWhere('description', 'like', '%' . $search_string . '%')->paginate(5);
            /*->get();*/
            return $data;
        }catch(\Exception $ex){
            $common     = new Common();
            $common->error_logging($ex, 'getSimpleDetails', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    /*
     * @return	single array on success with multiple array
    */
    public function getMasterRowWhere($tablename, $where = false)
    {
        $query = DB::table($tablename);
        if ($where) {
            if (is_array($where)) {
                foreach ($where as $key => $value) {
                    $query = $query->where($key, '=', $value);
                }
            }
        }
        return $query->first();
    }

    /**
     * master_insert method
     *
     * @access	public
     * @param	array data
     * @param	string tablename
     * @return	int last inserted id
     */
    public function insertData($request, $table_name)
    {
        try {
            $sessionData = session()->get('user_info');
            if(isset($sessionData) && !empty($sessionData)){
                $sessionData = session()->get('user_info');
                $created_by  = $sessionData['user_id'];
            }
            else{
                $created_by = '0';
            }

            $currentDate = Carbon::now();
            $request['created_by'] =  !empty($request['created_by']) ? $request['created_by'] :$created_by ;
            $request['updated_by'] =  !empty($request['updated_by']) ? $request['updated_by'] : $created_by;
            $request['created_at'] = $currentDate->toDateTimeString();
            $request['updated_at'] = $currentDate->toDateTimeString();
            //$request['is_deleted'] = 0;
            //$request['is_completed'] = 0;
            //echo "<pre>";print_r($request);die;
            $rows_affected = DB::table($table_name)->insert($request);

            return DB::getPdo()->lastInsertId();
        }catch(\Exception $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'insertData', 'MasterModel.php');
            // return view('layouts.coming_soon');
            print_r($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /**
     * get_master_row method
     *
     * @access	public
     * @param	string $tablename
     * @param	string $select
     * @param	string $where
     * @param	array $join
     * @return	single array on success
     */
    public function getMasterRow($tablename, $where = false)
    {
        try {
            if ($where) {
                foreach ($where as $key => $value) {
                    $data = DB::table($tablename)->orwhere($key, '=', $value)->first();
                }
            }
            return $data;
        }catch(\Exception $ex){
            $common     = new Common();
            $common->error_logging($ex, 'getMasterRow', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    /**
     * getMaster method
     *
     * @access	public
     * @param	array join
     * @param	string tablename
     * @param	string where
     * @return	array result set
     */
    public function getMaster($tablename, $select = false, $where = false, $like = false, $likeType = false, $orderBy = false, $join = false)
    {
        try {
            DB::enableQueryLog();

            if ($select) {
                $query = DB::table($tablename)->select($select);
            } else {
                $query = DB::table($tablename)->select('*');
            }

            if ($where) {
                if (is_array($where)) {
                    foreach ($where as $key => $value) {
                        $query = $query->where($key, '=', $value);
                    }
                } else {
                    $query = $query->whereRaw($where);
                }
            }

            /*
            @ $like       = ['master_code' => 'cnt', 'description' => 'ind'];
            @                column name => search_string
            @ $likeType   = 'or';
            @ Type of like (and, or)
            */

            if ($like && $likeType) {
                if (strtoupper($likeType) == 'AND') {
                    foreach ($like as $key => $value) {
                        //echo "<pre>";print_r($key);print_r($value);

                        $query = $query->Where($key, 'like', '%' . $value . '%');
                    }
                } else {
                    foreach ($like as $key => $value) {
                        //echo "<pre>";print_r($key);print_r($value);

                        $query = $query->orWhere($key, 'like', '%' . $value . '%');
                    }
                }
            }

            if ($orderBy) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
            if ($join) {
                if (count($join) > 0) {
                    foreach ($join as $key => $value) {
                        $explode = explode('|', $value);
                        print_r($key);
                        print_r($explode);
                        $query = $query->join($explode[0], $explode[1]);
                    }
                }
            }

            $data = $query->get();
            return $data;
            /*return response(array(
                'error' => false,
                'data'  => $data,
            ),200);*/
        }catch(\Exception $ex){
            // $common     = new Common();
            // $common->error_logging($ex, 'getMaster', 'MasterModel.php');
            // return view('layouts.coming_soon');
            print_r($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /**
     * updateData method
     *
     * @access	public
     * @param	string tablename
     * @param	string where
     * @return	update result set
     */
    public function updateData($data, $tablename, $where)
    {
        try{
            $currentDate    = Carbon::now();
            $sessionData    = session()->get('user_info');

            $query = DB::table($tablename);
            if ($where) {
                foreach ($where as $key => $value) {
                    $query = $query->where($key, $value);
                }
            }

            if(!isset($data['updated_by']) || empty($data['updated_by'])) {
                $data['updated_by'] =  isset($data['user_id']) ? $data['user_id'] : $sessionData['user_id'];
            }

            $data['updated_at'] = $currentDate->toDateTimeString();
            $rows_affected = $query->update($data);
            return $rows_affected;
        }catch (\Exception $ex) {
            $common     = new Common();
            $common->error_logging($ex, 'updateData', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    public function updateMasterData($request, $table_name)
    {
        try {
            $sessionData    = session()->get('user_info');
            $rows_affected = DB::table($table_name)
                ->where('id', $request->id)
                ->update([
                    'description' => $request->description,
                    'code' => $request->code,
                    'is_deleted' => $request->is_deleted,
                    'created_by' => '1',
                    'updated_by' => $sessionData['user_id']
                ]);
            return $rows_affected;
        }catch(\Exception $ex){
            $common     = new Common();
            $common->error_logging($ex, 'updateMasterData', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }
    public function destroyMasterData($request, $table_name)
    {
        try {
            $sessionData    = session()->get('user_info');
            $rows_affected = DB::table($table_name)
                ->where('id', $request->id)
                ->update([
                    'is_deleted' => 1,
                    'updated_by' => $sessionData['user_id']
                ]);
            return $rows_affected;
        }catch(\Exception $ex){
            $common     = new Common();
            $common->error_logging($ex, 'destroyMasterData', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    // get dropdown list data
    // if company id = 0 then user is super_admin
    public  function BindDDL($master_code, $company_id = 0, $is_deleted = 0, $parent_id = false, $orderby = 'ASC')
    {
        try {
            $data = DB::table('general_master')->select('id', 'description')
                ->where('master_code', '=', $master_code)
                ->where('company_id', '=', $company_id)
                ->where('is_deleted', '=', $is_deleted);
            if ($parent_id) {
                $data = $data->where('parent_id', '=', $parent_id);
            }
            $data->orderBy('description', $orderby);
            return $data->get();
        } catch (\Exception $ex) {
            $common     = new Common();
            $common->error_logging($ex, 'BindDDL', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    /*
     * This function return query with bind dynamic where conditions
    */
    public static function queryBinder($extraSettings = [], $query)
    {
        try {
            if (!empty($extraSettings['where']) && count($extraSettings['where']) > 0) {
                foreach ($extraSettings['where'] as $whereListKey => $whereList) {
                    if ($whereListKey == 'orWhere') {
                        $query->where(function ($q) use ($whereList) {
                            foreach ($whereList as $whereKey => $whereDetail) {
                                $q->OrWhere($whereDetail['column'], $whereDetail['expression'], $whereDetail['value']);
                            }
                        });
                    } else if ($whereListKey == 'where') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->where($whereDetail['column'], $whereDetail['expression'], $whereDetail['value']);
                        }
                    } else if ($whereListKey == 'whereIn') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->whereIn($whereDetail['column'], $whereDetail['value']);
                        }
                    } else if ($whereListKey == 'whereNotIn') {
                        foreach ($whereList as $whereKey => $whereDetail) {
                            $query->whereNotIn($whereDetail['column'], $whereDetail['value']);
                        }
                    }
                }
            }
            return $query;
        } catch (\Exception $ex) {
            $common     = new Common();
            $common->error_logging($ex, 'queryBinder', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }

    public static function getGeneralMasterData($request, $sessionData, $extraSettings)
    {
        $requestData = $request->all();

        if (!isset($extraSettings['select_columns']) || count($extraSettings['select_columns']) == 0) {
            $extraSettings['select_columns'] = ['general_master.*'];
        }

        try {
            $query          = DB::table('general_master')
                ->select($extraSettings['select_columns']);

            $query->where('general_master.id', '<>', 0);

            //Dynamic where condition added.
            $query          = MasterModel::queryBinder($extraSettings, $query);

            if (isset($extraSettings['order_by']) && count($extraSettings['order_by']) == 2) {
                $query->orderBy($extraSettings['order_by']['column'], $extraSettings['order_by']['order']);
            }
            if (!empty($extraSettings['fetch_method']) && $extraSettings['fetch_method'] == 'first') {
                return $query->first();
            } else {
                return $query->get();
            }
        } catch (\Exception $ex) {
            $common     = new Common();
            $common->error_logging($ex, 'getGeneralMasterData', 'MasterModel.php');
            return view('layouts.coming_soon');
        }
    }
}
